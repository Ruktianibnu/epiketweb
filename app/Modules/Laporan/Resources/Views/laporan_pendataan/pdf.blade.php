<!DOCTYPE html>
<html>
<head>
	<title>DAFTAR PENDATAAN TAMU PUSDAKIM</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<h5>Laporan Pendataan Tamu</h4>
	</center>
 
	<table border="1" class="A4" width="100%">
		<thead>
			<tr>
				<td width="3%"><strong>No</strong></td>
				<td width="10%"><strong>Nama
				Tamu/Vendor</strong></td>
				<td width="10%"><strong>Departement / Vendor</strong></td>
				<td width="10%"><strong>Lokasi</strong></td>
				<td width="10%"><strong>Kategori Kegiatan</strong></td>
				<td width="10%"><strong>Tanggal</strong></td>
				<td width="10%"><strong>Waktu</strong></td>
				<td width="10%"><strong>Deskripsi</strong></td>
				<td width="10%"><strong>Efek Kegiatan</strong></td>
				<td width="10%"><strong>Resiko Dan Solusi</strong></td>
				<td width="9%"><strong>Nama Petugas</strong></td>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($rs as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->nama_tamu}}</td>
				<td>{{$p->departement}}</td>
				<td>{{$p->lokasi}}</td>
				@if($p->lain_lain != null)
					<td>{{$p->kategori}} - {{$p->lain_lain}}</td>
				@else
					<td>{{$p->kategori}}</td>
				@endif
				<td>{{$p->tanggal_mulai}} - {{$p->tanggal_selesai}}</td>
				<td>{{$p->start_time}} - {{$p->end_time}}</td>
				<td>{{$p->deskripsi}}</td>
				<td>{{$p->efek}}</td>
				<td>{{$p->resiko}}</td>
				<td>{{$p->nama_petugas}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>