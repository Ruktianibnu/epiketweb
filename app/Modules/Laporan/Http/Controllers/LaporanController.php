<?php

namespace App\Modules\Laporan\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\model\laporan\laporan;

use Dompdf\Dompdf;
use PDF;
use DB;
use Hash;
use Image;
use File;
use Excel;
use Auth;
use Carbon\Carbon;
use App\Exports\LaporanExport;


class LaporanController extends Controller
{

	public function index() {

        $userLevel  = Auth::user()->level_pengguna;
        $id_petugas = Auth::user()->nip;

        $rs         = DB::table('pendataan_tamu as a')
                    ->select('a.id','b.nama as nama_petugas','a.nama as nama_tamu', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai')
                    ->join('users as b', 'a.id_petugas', 'b.nip')
                    ->paginate(10);

            return view('laporan::laporan_pendataan.index', ['rs' => $rs]);
    }

	/*public function export_excel(Request $request)
	{
		$data = null;
    	$lokasi  			= $request->cari_lokasi;
        $departement  		= $request->cari_departement;
        $kategori 			= $request->cari_kategori;
        $nama_petugas  		= $request->cari_nama_petugas;
        $tanggal_mulai  	= $request->cari_tanggal_mulai;
        $tanggal_selesai	= $request->cari_tanggal_selesai;
        $date 				= date('dd-mm-YYYY');

        if($request->has('cari_lokasi') || $request->has('cari_departement') || $request->has('cari_kategori') || $request->has('cari_nama_petugas') 
        	|| $request->has('cari_tanggal_mulai')){

        	if($tanggal_mulai != null && $tanggal_selesai != null){
            	$data     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai', 'pendataan_tamu.tanggal_selesai', 'pendataan_tamu.start_time', 'pendataan_tamu.end_time', 'pendataan_tamu.efek', 'pendataan_tamu.deskripsi', 'pendataan_tamu.resiko','a.lain_lain' )
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
	                    ->whereBetween('tanggal_mulai', [$tanggal_mulai, $tanggal_selesai])
	                    ->get();
            }
            else{
            	$data     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai', 'pendataan_tamu.tanggal_selesai', 'pendataan_tamu.start_time', 'pendataan_tamu.end_time', 'pendataan_tamu.efek', 'pendataan_tamu.deskripsi', 'pendataan_tamu.resiko','a.lain_lain' )
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
                    	->get();
            }

         	return Excel::create("Laporan Pendataan Tamu". '_' .date('Ymd').rand(1000, 9999), function ($excel) use ($data) {
            $excel->sheet('New sheet', function ($sheet) use ($data) {
                $sheet->loadView('laporan::lap_pendataan', ['data' => $data]);
                $sheet->setWidth([
                    'A' => 5, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 35, 'G' => 35, 'H' => 40, 'I' => 40, 'J' => 40, 'K' => 20,
                ]);
                $sheet->setFreeze('A6');
            
          });
            $lastrow = $excel->getActiveSheet()->getHighestRow();  
            $excel->getActiveSheet()->getStyle('A1:D'.$lastrow)->getAlignment()->setWrapText(true);
            })->export('xls');

        }else{
	        $data     = DB::table('pendataan_tamu as a')
                    ->select('a.id','b.nama as nama_petugas','a.nama as nama_tamu', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai', 'a.tanggal_selesai', 'a.start_time', 'a.end_time', 'a.efek', 'a.deskripsi', 'a.resiko', 'a.lain_lain')
                    ->join('users as b', 'a.id_petugas', 'b.nip')
                    ->get();

			 return Excel::download(new LaporanExport($data), "Laporan Pendataan"  . ' - ' . date('dmY') .'.xlsx');
        }
	}*/

	public function export_excel(Request $request) {
        $data     = Laporan::select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement', 'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai', 'pendataan_tamu.tanggal_selesai', 'pendataan_tamu.start_time', 'pendataan_tamu.end_time', 'pendataan_tamu.efek', 'pendataan_tamu.deskripsi', 'pendataan_tamu.resiko', 'pendataan_tamu.lain_lain')
                    ->join('users', 'pendataan_tamu.id_petugas', 'users.nip')
                    ->get();

            return Excel::download(new LaporanExport($data), "Laporan pendataan" .'.xlsx');
    }

	public function cetak_pdf(Request $request)
    {

    	$rs = null;
    	$lokasi  			= $request->cari_lokasi;
        $departement  		= $request->cari_departement;
        $kategori 			= $request->cari_kategori;
        $nama_petugas  		= $request->cari_nama_petugas;
        $tanggal_mulai  	= $request->cari_tanggal_mulai;
        $tanggal_selesai	= $request->cari_tanggal_selesai;
        $date 				= date('dd-mm-YYYY');

        if($request->has('cari_lokasi') || $request->has('cari_departement') || $request->has('cari_kategori') || $request->has('cari_nama_petugas') 
        	|| $request->has('cari_tanggal_mulai')){

        	if($tanggal_mulai != null && $tanggal_selesai != null){
            	$rs     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai', 'pendataan_tamu.tanggal_selesai', 'pendataan_tamu.start_time', 'pendataan_tamu.end_time', 'pendataan_tamu.efek', 'pendataan_tamu.deskripsi', 'pendataan_tamu.resiko','a.lain_lain' )
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
	                    ->whereBetween('tanggal_mulai', [$tanggal_mulai, $tanggal_selesai])
	                    ->get();
            }
            else{
            	$rs     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai', 'pendataan_tamu.tanggal_selesai', 'pendataan_tamu.start_time', 'pendataan_tamu.end_time', 'pendataan_tamu.efek', 'pendataan_tamu.deskripsi', 'pendataan_tamu.resiko','a.lain_lain' )
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
                    	->get();
            }

	        $pdf = PDF::loadview('laporan::laporan_pendataan.pdf', ['rs'=> $rs], [], ['orientation' => 'L', 'format' => 'A4-L', 'default_font' => 'arial']);
	    	return $pdf->stream('laporan_pendataan-'.$date.rand(1000, 9999).'.pdf');

        }else{
	        $rs     = DB::table('pendataan_tamu as a')
                    ->select('a.id','b.nama as nama_petugas','a.nama as nama_tamu', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai', 'a.tanggal_selesai', 'a.start_time', 'a.end_time', 'a.efek', 'a.deskripsi', 'a.resiko', 'a.lain_lain')
                    ->join('users as b', 'a.id_petugas', 'b.nip')
                    ->get();

			$pdf = PDF::loadview('laporan::laporan_pendataan.pdf', ['rs'=> $rs], [], ['orientation' => 'L', 'format' => 'A4-L', 'default_font' => 'arial']);

	        return $pdf->stream('laporan_pendataan-'.$date.rand(1000, 9999).'.pdf');
        }
    }

	

    public function search(Request $request) {
    	$lokasi  			= $request->cari_lokasi;
        $departement  		= $request->cari_departement;
        $kategori 			= $request->cari_kategori;
        $nama_petugas  		= $request->cari_nama_petugas;
        $tanggal_mulai  	= $request->cari_tanggal_mulai;
    	$tanggal_selesai	= $request->cari_tanggal_selesai;

        if($request->has('cari_lokasi') || $request->has('cari_departement') || $request->has('cari_kategori') || $request->has('cari_nama_petugas') 
        	|| $request->has('cari_tanggal_mulai')){

                if($tanggal_mulai != null && $tanggal_selesai != null){
                	$rs     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai')
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
	                    ->whereBetween('tanggal_mulai', [$tanggal_mulai, $tanggal_selesai])
	                    ->paginate(10);
                }
                else{
                	$rs     = DB::table('pendataan_tamu')
	                    ->select('pendataan_tamu.id','users.nama as nama_petugas','pendataan_tamu.nama as nama_tamu', 'pendataan_tamu.departement',
	                     'pendataan_tamu.lokasi', 'pendataan_tamu.kategori', 'pendataan_tamu.tanggal_mulai')
	                    ->join('users', 'users.nip', 'pendataan_tamu.id_petugas')
	                    ->where('lokasi','like',"%".$lokasi."%")
	                    ->where('departement','like',"%".$departement."%")
	                    ->where('kategori','like',"%".$kategori."%")
	                    ->where('users.nama','like',"%".$nama_petugas."%")
	                    ->paginate(10);
                }
	                    //dd($lokasi,$departement,$kategori,$nama_petugas,$tanggal_mulai,$tanggal_selesai);

            $rs->appends(['cari_lokasi' => $lokasi, 'cari_departement' => $departement, 'cari_kategori' => $kategori, 'cari_nama_petugas' => $nama_petugas]);
            return view('laporan::laporan_pendataan.index', ['rs' => $rs]);

        }else{
	        $rs     = DB::table('pendataan_tamu as a')
                    ->select('a.id','b.nama as nama_petugas','a.nama as nama_tamu', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai')
                    ->join('users as b', 'a.id_petugas', 'b.nip')
                    ->paginate(10);

            $rs->appends(['cari_lokasi' => $lokasi, 'cari_departement' => $departement, 'cari_kategori' => $kategori]);
            return view('laporan::laporan_pendataan.index', ['rs' => $rs]);
        }
    }

}