<?php

namespace App\Modules\Transaksi\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\model\transaksi\Pendataan;

use PDF;
use DB;
use Hash;
use Image;
use File;
use Auth;
use Carbon\Carbon;


class PendataanController extends Controller
{

    public function cetak_pdf($id)
    {
        $id_petugas = Auth::user()->nip;
        $datenow    = \Carbon\Carbon::now()->format('d-m-Y');
        //$rs         = pendataan::findOrFail($id);
        $rs         = DB::table('pendataan_tamu as a')
                    ->select('b.nama as nama_petugas','a.nama as nama', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai','a.lain_lain','a.deskripsi', 'a.jumlah', 'a.end_time', 'a.start_time', 'a.tanggal_selesai','a.efek','resiko')
                    ->join('users as b', 'a.id_petugas', 'b.nip')
                    ->where('a.id',$id)
                    ->get()->first();

                    //dd($rs);
        $pdf = PDF::loadview('transaksi::pendataan.laporan', ['rs'=> $rs]);
        return $pdf->stream();
    }

    public function index() {

        $userLevel  = Auth::user()->level_pengguna;
        $id_petugas = Auth::user()->nip;

        if($userLevel == '5') {
            $rs     = DB::table('pendataan_tamu as a')
                    ->select('a.id','a.nama', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai', 'a.lain_lain', 'b.nama as nama_petugas')
                    ->join('users as b', 'b.nip', 'a.id_petugas')
                    ->where('a.id_petugas',$id_petugas)
                    ->paginate(10);

            return view('transaksi::pendataan.index', ['rs' => $rs], ['level_pengguna'=>Auth::user()->level_pengguna]);
        }
        else{
            $rs     = DB::table('pendataan_tamu as a')
                    ->select('a.id','a.nama', 'a.departement', 'a.lokasi', 'a.kategori', 'a.tanggal_mulai', 'a.lain_lain', 'b.nama as nama_petugas')
                    ->join('users as b', 'b.nip', 'a.id_petugas')
                        ->paginate(10);
                      
            return view('transaksi::pendataan.index', ['rs' => $rs], ['level_pengguna'=>Auth::user()->level_pengguna]);
        }
    }

    public function page_add() {

        $userLevel  = Auth::user()->level_pengguna;
        // $userKantor = Auth::user()->kode_kantor;        

            return view('transaksi::pendataan.add');

    }

    public function store(Request $request) {

        $path_pemberitahuan     = null;
        $path_perintah          = null;
        $path_kegiatan          = null;
        $id_petugas             = Auth::user()->nip;
        $filetype_pemberitahuan = '';
        $filetype_perintah      = '';
        $filetype               = '';
        $extends                = '';

        if ($request->file('photo_pemberitahuan')) {
            $dir = public_path() . "/files/foto_pemberitahuan/$id_petugas";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            $filetype_pemberitahuan =$request->file('photo_pemberitahuan')->getClientOriginalExtension();
            $fileName = $id_petugas .date('Ymd').rand(1000,9999);
            $request->file('photo_pemberitahuan')->move($dir, $fileName .'.'. $filetype_pemberitahuan);
            $path_pemberitahuan = 'files/foto_pemberitahuan/' . $id_petugas . '/' . $fileName .'.'. $filetype_pemberitahuan;
        }

        if ($request->hasFile('photo_perintah')) {
            $dir = public_path() . "/files/foto_perintah/$id_petugas";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            $filetype_perintah = $request->file('photo_perintah')->getClientOriginalExtension();
            $fileName = $id_petugas .date('Ymd').rand(1000,9999);
            $request->file('photo_perintah')->move($dir, $fileName .'.'.$filetype_perintah);
            $path_perintah = 'files/foto_perintah/' . $id_petugas . '/' . $fileName .'.'. $filetype_perintah;
        }

        if ($request->hasFile('photo_kegiatan')) {
            $dir = public_path() . "/files/foto_kegiatan/$id_petugas";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            $filetype = $request->file('photo_kegiatan')->getClientOriginalExtension();
            
            if($filetype == 'jpg' || $filetype == 'png' || $filetype == 'jpeg' || $filetype == 'gif' || $filetype =='bmp' || $filetype == 'JPG' || $filetype == 'PNG' || $filetype == 'JPEG' || $filetype == 'GIF' || $filetype =='BMP'){
                $extends = 'true';
                 $fileName = $id_petugas .date('Ymd').rand(1000,9999);
                Image::make($request->file('photo_kegiatan'))->resize(150, 150)->save($dir . '/' . $fileName . '.jpg', 100);
                $path_kegiatan = 'files/foto_kegiatan/' . $id_petugas . '/' . $fileName . '.jpg';
            }else{
                
                $extends = 'false';
            }
        }

        if($extends == 'true'){
            $data                       = new Pendataan();
            $data->nama                 = $request->nama;
            $data->departement          = $request->departement;
            $data->jumlah               = $request->jumlah;
            $data->lokasi               = $request->lokasi;
            $data->tanggal_mulai        = $request->tanggal_mulai;
            $data->tanggal_selesai      = $request->tanggal_selesai;
            $data->start_time           = $request->waktu_mulai;
            $data->end_time             = $request->waktu_akhir;
            $data->kategori             = $request->kategori;
            $data->lain_lain            = $request->lain_lain;
            $data->deskripsi            = $request->deskripsi;
            $data->efek                 = $request->efek;
            $data->resiko               = $request->resiko;
            $data->id_petugas           = $id_petugas;
            $data->photo_pemberitahuan  = $path_pemberitahuan != null ? $path_pemberitahuan : null;
            $data->type_pemberitahuan   = $filetype_pemberitahuan;
            $data->photo_perintah       = $path_perintah != null ? $path_perintah : null;
            $data->type_perintah        = $filetype_perintah;
            $data->photo_perintah       = $path_perintah != null ? $path_perintah : null; 
            $data->photo_kegiatan       = $path_kegiatan != null ? $path_kegiatan : null; 
            $data->save();

            return response()->json(['status' => 'OK']);
        }else{
            return response()->json(['status' => 'Format tidak sesuai', 'message' => 'wrong']);
        }
    }

    public function page_show($id) {
        
        $rs     = Pendataan::findOrfail($id);

        //dd($rs);
        return view('transaksi::pendataan.show', ['rs' => $rs]);
    }

    public function edit(Request $request) {
        $path_pemberitahuan     = null;
        $path_perintah          = null;
        $path_kegiatan          = null;
        $path                   = null;
        $id                     = $request->id;
        $nip                    = Auth::user()->nip;
        $nip2                   = $request->nip;
        $filetype_pemberitahuan = '';
        $filetype_perintah      = '';
        $filetype               = '';
        $extends                = '';
        $data                   = Pendataan::find($id);

        if(!empty($request->file('photo_pemberitahuan'))){
             $dir = public_path() . "/files/foto_pemberitahuan/$nip2";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            $filetype_pemberitahuan =$request->file('photo_pemberitahuan')->getClientOriginalExtension();
            $fileName = $nip2 .date('Ymd').rand(1000,9999);
            $request->file('photo_pemberitahuan')->move($dir, $fileName .'.'. $filetype_pemberitahuan);
            $path_pemberitahuan = 'files/foto_pemberitahuan/' . $nip2 . '/' . $fileName .'.'. $filetype_pemberitahuan;

            $data->type_pemberitahuan   = $filetype_pemberitahuan;
            $data->photo_pemberitahuan  = $path_pemberitahuan != null ? $path_pemberitahuan : null;
        }

        if(!empty($request->hasFile('photo_perintah'))){
             $dir = public_path() . "/files/foto_perintah/$nip2";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            $filetype_perintah = $request->file('photo_perintah')->getClientOriginalExtension();
            $fileName = $nip2 .date('Ymd').rand(1000,9999);
            $request->file('photo_perintah')->move($dir, $fileName .'.'.$filetype_perintah);
            $path_perintah = 'files/foto_perintah/' . $nip2 . '/' . $fileName .'.'. $filetype_perintah;

            $data->photo_perintah       = $path_perintah != null ? $path_perintah : null; 
        }

        if(!empty($request->hasFile('photo_kegiatan'))){
            $dir = public_path() . "/files/foto_kegiatan/$nip2";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }

            $filetype = $request->file('photo_kegiatan')->getClientOriginalExtension();
            $fileName = $nip2 .date('Ymd').rand(1000,9999);
            Image::make($request->file('photo_kegiatan'))->resize(150, 150)->save($dir . '/' . $fileName . '.jpg', 100);
            $path_kegiatan = 'files/foto_kegiatan/' . $nip2 . '/' . $fileName . '.jpg';

            $data->photo_kegiatan       = $path_kegiatan != null ? $path_kegiatan : null;
        }

        if ($nip != $nip2) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'not_access' ] );
        } 

        $data->nama                 = $request->nama;
        $data->departement          = $request->departement;
        $data->jumlah               = $request->jumlah;
        $data->lokasi               = $request->lokasi;
        $data->tanggal_mulai        = $request->tanggal_mulai;
        $data->tanggal_selesai      = $request->tanggal_selesai;
        $data->start_time           = $request->waktu_mulai;
        $data->end_time             = $request->waktu_akhir;
        $data->kategori             = $request->kategori;
        $data->lain_lain            = $request->lain_lain;
        $data->deskripsi            = $request->deskripsi;
        $data->efek                 = $request->efek;
        $data->resiko               = $request->resiko;
        $data->id_petugas           = $nip;

        $data->save();

        return response()->json(['status' => 'OK']);
    }

    public function download_pemberitahuan($id){
       $model_file = Model::findOrFail($id); //Mencari model atau objek yang dicari
       $file = public_path() . $model_file->photo_pemberitahuan;//Mencari file dari model yang sudah dicari
       return response()->download($model_file->file_name); //Download file yang dicari berdasarkan nama file
    }

    public function download_perintah($id){
       $model_file = Model::findOrFail($id); //Mencari model atau objek yang dicari
       $file = public_path() . $model_file->photo_pemberitahuan;//Mencari file dari model yang sudah dicari
       return response()->download($model_file->file_name); //Download file yang dicari berdasarkan nama file
    }

    public function delete($id) {

        $pendataan                   = Pendataan::findOrfail($id);
        $pendataan->delete();

        return response()->json(['status' => 'OK']);

    }
}
