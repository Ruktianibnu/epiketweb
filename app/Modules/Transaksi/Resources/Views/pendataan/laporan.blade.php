<!DOCTYPE html>
<html lang="en">
<style type="text/css">
 
    #header{
        background:#0cf;
        padding:10px;
    }
    #content{
        padding:10px;
    }
    #footer{
        height:100px;
        line-height:50px;
        color:black;
    }
    .p {
      text-align: left;
      padding-top: 140px;
      font-size: 14px;
      position:absolute;
      bottom:0px;
    }
}
 
    </style>
        <head>
        <meta charset="utf-8">
        <title>Laporan Kegiatan Pusdakim</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    </head>
        <body class="A4">
        <section class="sheet padding-10mm">
            <h1 align="center">Laporan Kegiatan Pusdakim</h1>

            <table class="A4" width="100%">
                <thead>
                    <tr>
                        <td width="25%"><strong>Nama</strong></td>
                        <td width="30%">: {{$rs->nama}}</td>
                        <td width="20%"><strong>Lokasi</strong></td>
                        <td width="25%">: {{$rs->lokasi}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>Departement</strong></td>
                        <td>: {{$rs->departement}}</td>
                        <td><strong>Tanggal mulai</strong></td>
                        <td>: {{$rs->tanggal_mulai}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Tanggal sesesai</strong></td>
                        <td>: {{$rs->tanggal_selesai}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>Jumlah Orang</strong></td>
                        <td>: {{$rs->jumlah}}</td>
                        <td><strong>Jam Mulai</strong></td>
                        <td>: {{$rs->start_time}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Jam Selesai</strong></td>
                        <td>: {{$rs->end_time}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>Kategori Kegiatan</strong></td>
                        <td>: {{$rs->kategori}}</td>
                        <td><strong>Alasan lain-lain</strong></td>
                        @if($rs->lain_lain != null)
                            <td colspan="3">: {{$rs->lain_lain}}</td>
                        @else
                            <td colspan="3">: ..................</td>
                        @endif
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </thead>
            </table>

            <table width="100%">
                <thead>
                    <tr>
                        <td><strong>Deskripsi Kegiatan</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">{{$rs->deskripsi}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>Efek Yang Terjadi</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">{{$rs->efek}}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>Resiko Jika Gagal dan Solusi</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">{{$rs->resiko}}</td>
                    </tr>
                </thead>
            </table>
        </section>
        <p class="p">
            <a align="left">Petugas </a>
            <a align="left">: {{$rs->nama_petugas}}</a>
        </p>
    </body>
</html>