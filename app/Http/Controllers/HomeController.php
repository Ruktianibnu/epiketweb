<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\masterdata\Penerbangan;
use App\Model\masterdata\Tpi;
use App\Model\transaksi\Jamaah;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userLogin          = Auth::user()->id;
        $userKantor         = Auth::user()->kode_kantor;
        $userRole           = Auth::user()->level_pengguna;
        $userNIP            = Auth::user()->nip;

        // $ch = curl_init('http://10.0.30.35/passporttools/API/getOfficeBySearch');  
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['request' => json_encode(['username' => 'bcm', 'password' => 'bcmApi!2018', 'searchparam' => 'type', 'searchvalue' => 'kanim'])]));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); 
        // $exec = curl_exec($ch);
        // $kantor = json_decode($exec)->data;


        $countuser          = User::where('level_pengguna','<>','1')->count();/*
        $countkantor        = count($kantor);*/
        
        /*$countuser2         = User::where('level_pengguna','<>','1')->where('kode_kantor','=', $userKantor)->count();

        $countuser3         = User::where('level_pengguna','<>','1')->where('kode_kantor','=', $userKantor)->count();*/
        


        return view('home', ['countuser'=> $countuser]);


    }
}
