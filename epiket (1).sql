-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 11:31 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiket`
--

-- --------------------------------------------------------

--
-- Table structure for table `foto_pendataan_drc`
--

CREATE TABLE `foto_pendataan_drc` (
  `id_pendataan_drc` int(11) NOT NULL,
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(16, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(17, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(18, '2016_06_01_000004_create_oauth_clients_table', 1),
(19, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(20, '2019_05_23_022421_create_permission_tables', 1),
(21, '2019_05_23_041944_create_users_table', 1),
(22, '2019_05_23_061357_create_table_log_table', 1),
(23, '2019_10_17_040717_create_ms_subdirektorat_table', 1),
(24, '2019_11_14_020705_create_pendataan_tamu_table', 1),
(26, '2020_02_12_035011_create_foto_pendataan_drc_table', 1),
(27, '2020_02_17_101132_create_subdit_table', 1),
(28, '2020_02_18_063618_create_seksi_table', 1),
(29, '2020_02_11_071705_create_pendataan_drc_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 7),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 12),
(2, 'App\\User', 20),
(2, 'App\\User', 31),
(2, 'App\\User', 37),
(2, 'App\\User', 38),
(2, 'App\\User', 39),
(2, 'App\\User', 45),
(2, 'App\\User', 48),
(2, 'App\\User', 50),
(2, 'App\\User', 52),
(2, 'App\\User', 59),
(2, 'App\\User', 61),
(2, 'App\\User', 68),
(2, 'App\\User', 69),
(2, 'App\\User', 70),
(2, 'App\\User', 76),
(2, 'App\\User', 77),
(2, 'App\\User', 78),
(2, 'App\\User', 83),
(2, 'App\\User', 94),
(2, 'App\\User', 105),
(3, 'App\\User', 4),
(3, 'App\\User', 8),
(3, 'App\\User', 11),
(3, 'App\\User', 13),
(3, 'App\\User', 14),
(3, 'App\\User', 16),
(3, 'App\\User', 18),
(3, 'App\\User', 21),
(3, 'App\\User', 24),
(3, 'App\\User', 27),
(3, 'App\\User', 28),
(3, 'App\\User', 32),
(3, 'App\\User', 33),
(3, 'App\\User', 34),
(3, 'App\\User', 35),
(3, 'App\\User', 36),
(3, 'App\\User', 40),
(3, 'App\\User', 42),
(3, 'App\\User', 47),
(3, 'App\\User', 53),
(3, 'App\\User', 54),
(3, 'App\\User', 56),
(3, 'App\\User', 58),
(3, 'App\\User', 60),
(3, 'App\\User', 67),
(3, 'App\\User', 72),
(3, 'App\\User', 75),
(3, 'App\\User', 82),
(3, 'App\\User', 84),
(3, 'App\\User', 91),
(3, 'App\\User', 93),
(3, 'App\\User', 98),
(4, 'App\\User', 3),
(4, 'App\\User', 6),
(4, 'App\\User', 15),
(4, 'App\\User', 17),
(4, 'App\\User', 22),
(4, 'App\\User', 23),
(4, 'App\\User', 25),
(4, 'App\\User', 26),
(4, 'App\\User', 43),
(4, 'App\\User', 44),
(4, 'App\\User', 46),
(4, 'App\\User', 55),
(4, 'App\\User', 62),
(4, 'App\\User', 63),
(4, 'App\\User', 64),
(4, 'App\\User', 65),
(4, 'App\\User', 71),
(4, 'App\\User', 74),
(4, 'App\\User', 81),
(4, 'App\\User', 85),
(4, 'App\\User', 86),
(4, 'App\\User', 87),
(4, 'App\\User', 88),
(4, 'App\\User', 89),
(4, 'App\\User', 95),
(4, 'App\\User', 99),
(4, 'App\\User', 100),
(4, 'App\\User', 101),
(4, 'App\\User', 104),
(5, 'App\\User', 5),
(5, 'App\\User', 19),
(5, 'App\\User', 29),
(5, 'App\\User', 30),
(5, 'App\\User', 41),
(5, 'App\\User', 49),
(5, 'App\\User', 51),
(5, 'App\\User', 57),
(5, 'App\\User', 66),
(5, 'App\\User', 73),
(5, 'App\\User', 79),
(5, 'App\\User', 80),
(5, 'App\\User', 90),
(5, 'App\\User', 92),
(5, 'App\\User', 96),
(5, 'App\\User', 97),
(5, 'App\\User', 102),
(5, 'App\\User', 103);

-- --------------------------------------------------------

--
-- Table structure for table `ms_subdirektorat`
--

CREATE TABLE `ms_subdirektorat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip_kasubdit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pendataan_drc`
--

CREATE TABLE `pendataan_drc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `petugas_1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas_2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suhu_ruangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_backup_1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_backup_2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ups_redudancy` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ups_load` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ups_runtime` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ups_temp` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pendataan_tamu`
--

CREATE TABLE `pendataan_tamu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `lokasi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lain_lain` text COLLATE utf8mb4_unicode_ci,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `efek` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `resiko` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_petugas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_pemberitahuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_pemberitahuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_perintah` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_perintah` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_kegiatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pendataan_tamu`
--

INSERT INTO `pendataan_tamu` (`id`, `nama`, `departement`, `jumlah`, `lokasi`, `tanggal_mulai`, `tanggal_selesai`, `start_time`, `end_time`, `kategori`, `lain_lain`, `deskripsi`, `efek`, `resiko`, `id_petugas`, `photo_pemberitahuan`, `type_pemberitahuan`, `photo_perintah`, `type_perintah`, `photo_kegiatan`, `created_at`, `updated_at`) VALUES
(1, 'adri', 'prosia', 1, 'Pusdakim 1 lantai 2', '2020-02-19', '2020-02-19', '09:00:00', '10:00:00', 'Install', NULL, '-', '-', '-', 'superadmin', NULL, '', NULL, '', 'files/foto_kegiatan/superadmin/superadmin202002195287.jpg', '2020-02-19 02:28:36', '2020-02-19 02:28:36'),
(2, 'dodo', 'signet', 3, 'Pusdakim 1 lantai 2', '2020-02-20', '2020-02-20', '12:00:00', '13:00:00', 'Install', NULL, 'instal app', 'app berjalan semestinya', 'app tidak bisa di akses', '111111111111111111', 'files/foto_pemberitahuan/111111111111111111/111111111111111111202002209211.pdf', 'pdf', 'files/foto_perintah/111111111111111111/111111111111111111202002206811.pdf', 'pdf', 'files/foto_kegiatan/111111111111111111/111111111111111111202002202243.jpg', '2020-02-19 20:43:15', '2020-02-19 20:43:51');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Melihat daftar pendataan', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(2, 'Menambah data pendataan', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(3, 'Mengubah data pendataan', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(4, 'Semua proses pendataan', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(5, 'Aksi master', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(6, 'Mencetak seluruh Laporan', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'SUPERADMIN', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(2, 'DIREKTUR', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(3, 'KASUBDIT', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(4, 'KASI', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(5, 'PETUGAS', 'web', '2020-02-19 01:40:01', '2020-02-19 01:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 4),
(1, 5),
(2, 1),
(2, 4),
(2, 5),
(3, 1),
(3, 4),
(3, 5),
(4, 1),
(4, 4),
(4, 5),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `seksi`
--

CREATE TABLE `seksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_seksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_seksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seksi`
--

INSERT INTO `seksi` (`id`, `kode_seksi`, `nama_seksi`, `kode_subdirektorat`, `created_at`, `updated_at`) VALUES
(1, 'renkim', 'Perencanaan Simkim', 'renbang', '2020-02-19 01:43:39', '2020-02-19 01:43:39'),
(2, 'bangkim', 'Pengembangan Simkim', 'renbang', '2020-02-19 01:43:59', '2020-02-19 01:43:59'),
(3, 'dayaguna', 'Dayaguna Simkim', 'renbang', '2020-02-19 01:44:11', '2020-02-19 01:44:11'),
(4, 'wil 1', 'wilayah 1', 'harman', '2020-02-19 01:44:23', '2020-02-19 01:44:23'),
(5, 'wil 2', 'wilayah 2', 'harman', '2020-02-19 01:44:31', '2020-02-19 01:44:31'),
(6, 'wil 3', 'wilayah 3', 'harman', '2020-02-19 01:44:47', '2020-02-19 01:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `subdit`
--

CREATE TABLE `subdit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subdit`
--

INSERT INTO `subdit` (`id`, `kode_subdirektorat`, `nama_subdirektorat`, `created_at`, `updated_at`) VALUES
(1, 'renbang', 'Perencanaan Dan Pengembangan', '2020-02-19 01:42:08', '2020-02-19 01:42:08'),
(2, 'harman', 'Pemeliharaan Dan Pengamanan', '2020-02-19 01:42:21', '2020-02-19 01:42:21'),
(3, 'jasmatik', 'Kerja Sama Pemanfaatan Teknologi Informasi Keimigrasian', '2020-02-19 01:43:02', '2020-02-19 01:43:02'),
(4, 'pdp', 'Pelaporan Dan Pendataan', '2020-02-19 01:43:21', '2020-02-19 01:43:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log`
--

CREATE TABLE `tbl_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `log` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_subdirektorat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_seksi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aktif` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_pengguna` smallint(6) NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nip`, `nama`, `no_hp`, `password`, `kode_subdirektorat`, `kode_seksi`, `aktif`, `level_pengguna`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', NULL, '$2y$10$JRpVSYEqVN7X9SsiPmFTEe9axbzijp5E1pdp1C4nGZZ6M/9fV0iI2', 'RENBANG', NULL, '1', 1, NULL, NULL, '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(2, '199507252019011001', 'Ruktian Ibnu Wijonarko', NULL, '$2y$10$JRpVSYEqVN7X9SsiPmFTEe9axbzijp5E1pdp1C4nGZZ6M/9fV0iI2', 'renbang', 'renkim', '1', 4, NULL, NULL, '2020-02-19 01:40:01', '2020-02-19 01:40:01'),
(102, '199404022019012001', 'Hana Aprillia Dahrani', '081283628065', '$2y$10$f522iqMF8ACYe2D10gukYuiHyXML0yDn1H0KXXo2pmMqg2CSWNEtS', 'renbang', 'bangkim', '1', 5, NULL, NULL, '2020-02-19 03:07:36', '2020-02-19 20:17:12'),
(103, '111111111111111111', 'test petugas', '1111', '$2y$10$QuSnyNhDGwwgI1zZXgv/vexDgtfiKhHcU6Vhal0WAtGSX.St4pE0G', 'harman', 'wil 1', '1', 5, NULL, NULL, '2020-02-19 20:41:01', '2020-02-19 20:41:18'),
(104, '222222222222222222', 'Cakra Trinata', '1212', '$2y$10$JxswhZacwjZGHB5tizuu5OhqVZ2auwGnxVsbtfoKyojjlCi8/3aOK', 'renbang', 'renkim', '1', 4, NULL, NULL, '2020-02-19 20:54:04', '2020-02-19 20:54:15'),
(105, '199999999999999999', 'agato', '1212', '$2y$10$GlqWvUeZ7sBu5l/4qsHB8eNNQcwHrDq2vhdR8OTt/rlqH/K3y1b3q', NULL, NULL, '1', 2, NULL, NULL, '2020-02-19 20:58:27', '2020-02-19 20:58:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `ms_subdirektorat`
--
ALTER TABLE `ms_subdirektorat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ms_subdirektorat_kode_subdirektorat_unique` (`kode_subdirektorat`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pendataan_drc`
--
ALTER TABLE `pendataan_drc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendataan_tamu`
--
ALTER TABLE `pendataan_tamu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `seksi`
--
ALTER TABLE `seksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subdit`
--
ALTER TABLE `subdit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subdit_kode_subdirektorat_unique` (`kode_subdirektorat`);

--
-- Indexes for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_log_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nip_unique` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `ms_subdirektorat`
--
ALTER TABLE `ms_subdirektorat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pendataan_drc`
--
ALTER TABLE `pendataan_drc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pendataan_tamu`
--
ALTER TABLE `pendataan_tamu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `seksi`
--
ALTER TABLE `seksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subdit`
--
ALTER TABLE `subdit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_log`
--
ALTER TABLE `tbl_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD CONSTRAINT `tbl_log_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
